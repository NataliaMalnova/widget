const placeholder = (inputSelectior, valueUpdate) => {
  
    try{
        const input = document.querySelector(inputSelectior);
        const update = input.parentNode.querySelector('.updateText');

        const pattern = /^\d+(\.?)\d*$/g;
        const allowedCodes = [8, 9, 27, 35, 36, 37, 38, 39, 46, 110, 188];
    
        input.addEventListener('input', onInput);
    
        function onInput(e) {
          const value = this.value;
          
          if (input.value.length == 0) {
            update.innerHTML = valueUpdate;
            //update.classList.remove('updateTextCurcor');
          } else {
            update.classList.add('updateTextCurcor');
            if( !(value.match(pattern) || allowedCodes.some(code => code === e.keyCode)) ) {
              this.value = value.slice(0, -1);
            }
            
            let mantissa = input.value.split('.');
            if(mantissa[1] == undefined){
              //update.innerHTML = mantissa[0];
            } else{
              update.innerHTML = mantissa[0] + '.' + 
              '<span>' + mantissa[1] +'</span>';
            }

            if (input.value == '') {
              update.innerHTML = valueUpdate;
            }
          }
        }
      }catch(e){}

};


const placeholderLong = (inputSelectior, valueUpdate) => {
  
    try{
        const input = document.querySelector(inputSelectior);
        const update = input.parentNode.querySelector('.updateText');

        input.addEventListener('input', onInput);
    
        function onInput(e) {
          const value = this.value;
          
          if (input.value.length == 0) {
            update.innerHTML = valueUpdate;
        } else {
            update.innerHTML = '';
          }
        }
      }catch(e){}

};