//  СДЕЛАТЬ КНОПКУ ЗЕЛЕНОЙ ПОСЛЕ ВВОДА ПРАВИЛЬНОГО ЗНАЧЕНИЯ

const userCoude = (coudeSelectior, validValue, btnSelector) => {

    try{

        const userCoude = document.querySelector(coudeSelectior);
        const btn = document.querySelector(btnSelector);
        
        userCoude.addEventListener('input', function(e) {

            if (userCoude.value == validValue) {  
                btn.classList.remove('btn__grey');
                btn.classList.add('btn__green');

                if(userCoude.id == 'bitcoinAddress'){
                    document.querySelector('.detail .btn__pay').classList.remove('btn__black');
                    document.querySelector('.detail .btn__pay').classList.add('btn__black');
                }
            } else{
                btn.classList.remove('btn__green');
                btn.classList.add('btn__grey');
            }
        

          });
    } catch(e) {}

};

//  ВВОД ДО ОПРЕДЕЛЕННОГО ЗНАЧЕНИЯ

const inputValidValue = (inputNumber, maxValue) => {  
    
    const num = document.querySelectorAll(inputNumber);
    const numMax = maxValue;
  
    num.forEach(item => {
        item.addEventListener('input', (e) => {
            item.value = item.value.replace (/\D/g, '');
            if (item.value > numMax) {
                item.value = numMax;
            } 
        });
    });
    
};
  
  //  ТОЛЬКО ЦИФРЫ
  
const inputValidNum = (inputNumber) => {  
      
    const num = document.querySelectorAll(inputNumber);
    num.forEach(item => {
        item.addEventListener('input', (e) => {
                item.value = item.value.replace (/\D/g, '');
        });
    });
    
};
  